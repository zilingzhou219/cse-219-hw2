package pm.data;

import java.util.ArrayList;
import java.util.Iterator;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import pm.PoseMaker;
import pm.controller.PageEditController;
import pm.gui.Workspace;
import saf.components.AppDataComponent;
import saf.AppTemplate;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    PageEditController pec;
    //Place where we store shapes
    ArrayList<Shape> shapelist = new ArrayList<>();
    Color backgroundC = Color.web("#aaaaff");
    Color currentFill = Color.RED;
    Color currentStrok = Color.BLUE;
    double currentSliderValue = 20;
    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
	// KEEP THE APP FOR LATER
	app = initApp;
    }
    public ArrayList<Shape> getShapeList(){
        return shapelist;
    }
    public void setBackgroundC(Color c){
        backgroundC = c;
    }
    public Color getBackgroundC(){
        return backgroundC;
    }
    
    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void reset() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        pec = workspace.getPec();
        workspace.getSelectionTool().setDisable(false);
        workspace.getRemove().setDisable(true);
        //set background color
        workspace.getBackgroundPicker().setValue(backgroundC);
        workspace.getRightPane().setBackground(new Background(new BackgroundFill(getBackgroundC(), CornerRadii.EMPTY, Insets.EMPTY)));
        //now add all the shape to the pane.
       workspace.getRightPane().getChildren().clear();
       workspace.getRightPane().getChildren().addAll(0, shapelist);
       for(int i = 0; i < shapelist.size(); i++){
           Shape shape = shapelist.get(i);
           shape.setOnMouseClicked(e -> pec.handleShapeSelected(e, shape, workspace.getRightPane()));           
           shape.setOnMouseDragged(e -> pec.handleShapeSelected(e, shape, workspace.getRightPane()));
       }   
    }
    /*
    * function to change the outline color when saving
    */
    public void changeLastColor(){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        if (workspace.getPec().getPreIndex() >= 0)
            workspace.getPec().handlePreSelect();
     //   workspace.getPec().setSelectionMode(false);
     //   workspace.getSelectionTool().setDisable(false);
      //   System.out.println(workspace.getSelectionTool().isDisable());
        workspace.getPec().setCurrentSelected(null);
        workspace.getPec().foolProof_Select_Remove();
        workspace.getPec().setSelectionMode(false);
        workspace.getSelectionTool().setDisable(false);
 
//        workspace.getSelectionTool().setDisable(false);
         
        //wor
//        workspace.getPec().foolProof_Select_Remove();
    //    System.out.println("come here");
    }
    
    /**
     * @return the currentFill
     */
    public Color getCurrentFill() {
        return currentFill;
    }

    /**
     * @param currentFill the currentFill to set
     */
    public void setCurrentFill(Color currentFill) {
        this.currentFill = currentFill;
    }

    /**
     * @return the currentStrok
     */
    public Color getCurrentStrok() {
        return currentStrok;
    }

    /**
     * @param currentStrok the currentStrok to set
     */
    public void setCurrentStrok(Color currentStrok) {
        this.currentStrok = currentStrok;
    }

    /**
     * @return the currentSliderValue
     */
    public double getCurrentSliderValue() {
        return currentSliderValue;
    }

    /**
     * @param currentSliderValue the currentSliderValue to set
     */
    public void setCurrentSliderValue(double currentSliderValue) {
        this.currentSliderValue = currentSliderValue;
    }

}
