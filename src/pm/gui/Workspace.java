package pm.gui;

import java.io.IOException;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Shape;
import pm.PoseMaker;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
import static pm.PropertyType.ELLIPSE_ICON;
import static pm.PropertyType.ELLIPSE_TOOLTIP;
import static pm.PropertyType.MOVE_TO_BACK_ICON;
import static pm.PropertyType.MOVE_TO_BACK_TOOLTIP;
import static pm.PropertyType.MOVE_TO_FRONT_ICON;
import static pm.PropertyType.MOVE_TO_FRONT_TOOLTIP;
import static pm.PropertyType.RECTANGLE_ICON;
import static pm.PropertyType.RECTANGLE_TOOLTIP;
import static pm.PropertyType.REMOVE_ICON;
import static pm.PropertyType.REMOVE_TOOLTIP;
import static pm.PropertyType.SELECTION_TOOL_ICON;
import static pm.PropertyType.SELECTION_TOOL_TOOLTIP;
import static pm.PropertyType.SNAPSHOT_ICON;
import static pm.PropertyType.SNAPSHOT_TOOLTIP;
import pm.controller.PageEditController;
import pm.data.DataManager;
import properties_manager.PropertiesManager;
public class Workspace extends AppWorkspaceComponent {

    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;
    
    //Event handlers
    PageEditController pec;
    
    //Those are the workspace pane
    BorderPane border;
    VBox leftPane; //the tool bar 
        HBox btnSelection;
            Button  drawRect,drawEllipse,remove,selectionTool;
        HBox upDown;
            Button up,down;
        VBox backgroundPane;
            Label backgroundLbl;
            ColorPicker backgroundPicker;
        VBox fillPane;
            Label fillLbl;
            ColorPicker fillPicker;
        VBox outlineColorPane;
            Label outlineLbl;
            ColorPicker outlinePicker;
        VBox outlineThickPane;
            Label thickLbl;
            Slider strokeThickness;
        StackPane snapPane;
            Button snap;
        Region spacer;    
    Pane rightPane; //the drawing pane
    
    //Those are the compoents in the tool bar
    
    
    
    
    
 
    
    
    
    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();
        
        PropertiesManager proManager = PropertiesManager.getPropertiesManager();
        pec = new PageEditController((PoseMaker)app);
        DataManager dataManager = (DataManager) app.getDataComponent();
        
        workspace = new Pane();
        border = new BorderPane();
            leftPane = new VBox();
                    
                btnSelection = new HBox();
                    selectionTool = gui.initChildButton(btnSelection, SELECTION_TOOL_ICON.toString(),SELECTION_TOOL_TOOLTIP.toString() , true);
                    remove = gui.initChildButton(btnSelection, REMOVE_ICON.toString(),REMOVE_TOOLTIP.toString() , true);
                    drawRect =  gui.initChildButton(btnSelection, RECTANGLE_ICON.toString(),RECTANGLE_TOOLTIP.toString() , false);
                    drawEllipse = gui.initChildButton(btnSelection, ELLIPSE_ICON.toString(),ELLIPSE_TOOLTIP.toString() , false);
    //            System.out.println(selectionTool.isDisable());    
                //selection tool and remove are both setDisable foolProof
                    selectionTool.setDisable(true);
    //                System.out.println(remove.isDisable());
               //     remove.setDisable(true);
                    //set action event for nodes inside btnSelectio
                    selectionTool.setOnAction(e -> pec.handleSelectionTool());
                    remove.setOnAction(e -> pec.handleRemove(rightPane));
                    drawRect.setOnAction(e -> pec.handleDrawRectangle());
                    drawEllipse.setOnAction(e -> pec.handleDrawEllipse());
                    
                upDown = new HBox();
                    up = gui.initChildButton(upDown, MOVE_TO_FRONT_ICON.toString(),MOVE_TO_FRONT_TOOLTIP.toString() , true);
                    down = gui.initChildButton(upDown, MOVE_TO_BACK_ICON.toString(),MOVE_TO_BACK_TOOLTIP.toString() , true);
                    up.prefWidthProperty().bind(upDown.widthProperty().divide(3));
                    up.setOnAction(e -> pec.moveToFront(rightPane));
                    down.prefWidthProperty().bind(upDown.widthProperty().divide(3));
                    down.setOnAction(e -> pec.moveToBack(rightPane));
                    upDown.setAlignment(Pos.CENTER);
         //           up.setDisable(true);
         //           down.setDisable(true);
                    
                    
                backgroundPane = new VBox();
                    backgroundLbl = new Label("Background Color");
                    backgroundPicker = new ColorPicker(dataManager.getBackgroundC());
                    backgroundPicker.setOnAction(e -> pec.handleBackgroundColor(backgroundPicker,rightPane));
                    backgroundPane.getChildren().addAll(backgroundLbl,backgroundPicker);
                
                fillPane = new VBox();
                    fillLbl = new Label("Fill Color");
                    fillPicker = new ColorPicker(dataManager.getCurrentFill());
                    fillPicker.setOnAction(e -> pec.handleFillRect(fillPicker));
                    fillPane.getChildren().addAll(fillLbl,fillPicker);
                
                outlineColorPane = new VBox();
                    outlineLbl = new Label("Outline Color");
                    outlinePicker = new ColorPicker(dataManager.getCurrentStrok());
                    outlinePicker.setOnAction(e -> pec.handleOutlineColor(outlinePicker));
                    outlineColorPane.getChildren().addAll(outlineLbl,outlinePicker);
                
                outlineThickPane = new VBox();
                    thickLbl = new Label("Outline Thickness");
                    strokeThickness = new Slider();
                    strokeThickness.setValue(dataManager.getCurrentSliderValue());
                    strokeThickness.setOnMouseDragged(e -> pec.handleOutlineThickness(strokeThickness));
                    outlineThickPane.getChildren().addAll(thickLbl,strokeThickness);
                    
                snapPane = new StackPane();
                    snap = gui.initChildButton(snapPane, SNAPSHOT_ICON.toString(),SNAPSHOT_TOOLTIP.toString() , false);
                    snap.setOnAction(e -> pec.handleSnapShot(rightPane));
                    snapPane.setAlignment(Pos.CENTER);
        /*            
            spacer = new Region();
           
            spacer.setPrefHeight(border.getHeight());
            spacer.prefHeight(border.getHeight());
            spacer.prefHeightProperty().bind(leftPane.heightProperty()
                    .subtract(btnSelection.heightProperty())
                    .subtract(upDown.heightProperty())
                    .subtract(backgroundPane.heightProperty())
                    .subtract(fillPane.heightProperty()
                    .subtract(outlineColorPane.heightProperty())
                    .subtract(outlineThickPane.heightProperty())
                    .subtract(snapPane.heightProperty())
                    ));
            spacer.setMaxHeight(200); */
            leftPane.getChildren().addAll(btnSelection,upDown,backgroundPane,fillPane,outlineColorPane,outlineThickPane,snapPane);
            leftPane.prefHeightProperty().bind(workspace.heightProperty());
            
            rightPane = new Pane();
            rightPane.getChildren().add(pec.getRect());
            rightPane.getChildren().add(pec.getEllip());
            rightPane.setOnMouseDragged(e -> pec.handleDrawShape(rightPane,e));
            rightPane.setOnMousePressed(e -> pec.handleDrawShape(rightPane, e));
            rightPane.setOnMouseReleased(e -> pec.handleDrawShape(rightPane, e));
            rightPane.setOnMouseEntered(e -> pec.handleDrawShape(rightPane, e));
            rightPane.setOnMouseExited(e -> pec.handleDrawShape(rightPane, e));
            
            //!!!!!!!!! This line makes right pane work!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       
            border.prefHeightProperty().bind(workspace.heightProperty());
            border.prefWidthProperty().bind(workspace.widthProperty());
          
            border.setLeft(leftPane);
            border.setCenter(rightPane);
     // selectionTool.setDisable(true);
//      System.out.println(selectionTool.isDisable());
     workspace.getChildren().addAll(border);
    }
    public Pane getRightPane(){
        return rightPane;
    }
    
    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
        //Pane
        leftPane.getStyleClass().add("bordered_pane");
        rightPane.getStyleClass().add("bordered_pane");
        btnSelection.getStyleClass().add("bordered_pane");
        upDown.getStyleClass().add("bordered_pane");
        backgroundPane.getStyleClass().add("bordered_pane");
        fillPane.getStyleClass().add("bordered_pane");
        outlineColorPane.getStyleClass().add("bordered_pane");
        outlineThickPane.getStyleClass().add("bordered_pane");
        snapPane.getStyleClass().add("bordered_pane");
 //       spacer.getStyleClass().add("spacer_color");
        //text
       backgroundLbl.getStyleClass().add("heading_label");
       fillLbl.getStyleClass().add("heading_label");
       outlineLbl.getStyleClass().add("heading_label");
       thickLbl.getStyleClass().add("heading_label");
        //color chooser
        getBackgroundPicker().getStyleClass().add("button");
        getFillPicker().getStyleClass().add("button");
        getOutlinePicker().getStyleClass().add("button");
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {
/*        //IMPORTANT NEXT TIME PUT WHAT I DID IN DATAMANAGER INTO RELOADWORKSPACE
    //    Workspace workspace = (Workspace)app.getWorkspaceComponent();
    //    pec = workspace.getPec();
        DataManager dataManager = (DataManager)app.getDataComponent();
        getSelectionTool().setDisable(false);
        //set background color
        getBackgroundPicker().setValue(dataManager.getBackgroundC());
        getRightPane().setBackground(new Background(new BackgroundFill(dataManager.getBackgroundC(), CornerRadii.EMPTY, Insets.EMPTY)));
        //now add all the shape to the pane.
       getRightPane().getChildren().clear();
       getRightPane().getChildren().addAll(0, dataManager.getShapeList());
       for(int i = 0; i < dataManager.getShapeList().size(); i++){
           Shape shape = dataManager.getShapeList().get(i);
           shape.setOnMouseClicked(e -> pec.handleShapeSelected(e, shape, getRightPane()));           
           shape.setOnMouseDragged(e -> pec.handleShapeSelected(e, shape, getRightPane()));
       }
  */  }

    /**
     * @return the remove
     */
    public Button getRemove() {
        return remove;
    }

    /**
     * @return the selectionTool
     */
    public Button getSelectionTool() {
        return selectionTool;
    }

    /**
     * @return the drawRect
     */
    public Button getDrawRect() {
        return drawRect;
    }

    /**
     * @return the drawEllipse
     */
    public Button getDrawEllipse() {
        return drawEllipse;
    }

    /**
     * @return the fillPicker
     */
    public ColorPicker getFillPicker() {
        return fillPicker;
    }

    /**
     * @return the outlinePicker
     */
    public ColorPicker getOutlinePicker() {
        return outlinePicker;
    }

    /**
     * @return the strokeThickness
     */
    public Slider getStrokeThickness() {
        return strokeThickness;
    }

    /**
     * @return the up
     */
    public Button getUp() {
        return up;
    }

    /**
     * @return the down
     */
    public Button getDown() {
        return down;
    }

    /**
     * @return the backgroundPicker
     */
    public ColorPicker getBackgroundPicker() {
        return backgroundPicker;
    }

    /**
     * @param backgroundPicker the backgroundPicker to set
     */
    public void setBackgroundPicker(ColorPicker backgroundPicker) {
        this.backgroundPicker = backgroundPicker;
    }

    /**
     * @return the pec
     */
    public PageEditController getPec() {
        return pec;
    }

    /**
     * @return the gui
     */
    public AppGUI getGui() {
        return gui;
    }
    
}
