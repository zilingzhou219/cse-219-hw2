/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.controller;

import java.io.IOException;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.shape.StrokeLineCap;
import pm.PoseMaker;
import pm.data.DataManager;
import pm.file.FileManager;
import pm.gui.Workspace;

/**
 *
 * @author ziling
 */
public class PageEditController {
    PoseMaker app;
    boolean selectionMode;
    //true when user wants to select items, false otherwise
    boolean rectangleMode,ellipseMode;
    Rectangle rect = new Rectangle();
    SimpleDoubleProperty initX = new SimpleDoubleProperty();
    SimpleDoubleProperty initY = new SimpleDoubleProperty();
    SimpleDoubleProperty finalX = new SimpleDoubleProperty();
    SimpleDoubleProperty finalY = new SimpleDoubleProperty(); 
    
    Ellipse ellip = new Ellipse();
    SimpleDoubleProperty initRadiusX = new SimpleDoubleProperty();
    SimpleDoubleProperty initRadiusY = new SimpleDoubleProperty();
    SimpleDoubleProperty finalRadiusX = new SimpleDoubleProperty();
    SimpleDoubleProperty finalRadiusY = new SimpleDoubleProperty();
    //cursor used for this project
    Shape currentSelected;
    int preIndex = -1;
    Color preColor = null;
    public PageEditController(PoseMaker initmaker){
        app = initmaker;
        
        //Make sure out rectangle changes simutanously 
          rect.xProperty().bind(initX);
          rect.yProperty().bind(initY);
          rect.widthProperty().bind(finalX.subtract(initX));
          rect.heightProperty().bind(finalY.subtract(initY));
          
          ellip.centerXProperty().bind(initRadiusX);
          ellip.centerYProperty().bind(initRadiusY);
          ellip.radiusXProperty().bind(finalRadiusX.subtract(initRadiusX));
          ellip.radiusYProperty().bind(finalRadiusY.subtract(initRadiusY));
    }
    public Rectangle getRect(){
        return rect;
    }
    public Ellipse getEllip(){
        return ellip;
    }
    public void handleDrawRectangle(){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        DataManager dataManager = (DataManager) app.getDataComponent();
        
        setSelectionMode(false);
        rectangleMode = true;
        ellipseMode = false;
        setCurrentSelected(null);
    
        foolProof_Select_Remove();
        if(getPreIndex() >= 0)
            handlePreSelect();
        
        workspace.getDrawEllipse().setDisable(false);
        workspace.getDrawRect().setDisable(true);        
    }
    /*
    * Handle whether remove and selection should be open.
    */
    public void foolProof_Select_Remove(){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        DataManager dataManager = (DataManager) app.getDataComponent();
   //     System.out.println("come here");
        //This will manager for the selection tool 
        if(dataManager.getShapeList().size() > 0){
            workspace.getRemove().setDisable(false);
            workspace.getSelectionTool().setDisable(false);
        }else if((dataManager.getShapeList().size() < 1)){
            workspace.getRemove().setDisable(true); 
            workspace.getSelectionTool().setDisable(true);
        }
        //up down button
        if(currentSelected == null){
            workspace.getUp().setDisable(true);
            workspace.getDown().setDisable(true);
            workspace.getRemove().setDisable(true);
        }else if(currentSelected != null){
            workspace.getUp().setDisable(false);
            workspace.getDown().setDisable(false);
             workspace.getSelectionTool().setDisable(true);
            int index = dataManager.getShapeList().indexOf(currentSelected);
            if(index == dataManager.getShapeList().size() - 1){
                workspace.getUp().setDisable(true);
            }
            if(index == 0){
                workspace.getDown().setDisable(true);
            }          
        }
        
        
    //    System.out.println("pane size: " + workspace.getRightPane().getChildren().size());
    //    System.out.println(dataManager.getShapeList().size());
    }
    
    public void handleDrawEllipse(){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        DataManager dataManager = (DataManager) app.getDataComponent();
        
        setSelectionMode(false);
        ellipseMode = true;
        rectangleMode = false;
        setCurrentSelected(null);
        
        foolProof_Select_Remove();
        if(getPreIndex() >= 0)
            handlePreSelect();
        
        workspace.getDrawEllipse().setDisable(true);
        workspace.getDrawRect().setDisable(false);
    }
    public void handleSelectionTool(){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        setSelectionMode(true);
        rectangleMode = false;
        ellipseMode = false;
        setCurrentSelected(null);
        foolProof_Select_Remove();
        workspace.getSelectionTool().setDisable(true);
        workspace.getDrawEllipse().setDisable(false);
        workspace.getDrawRect().setDisable(false);
    }
    public void handleRemove(Pane rightPane){
        DataManager dataManager = (DataManager) app.getDataComponent();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        
        rightPane.getChildren().remove(currentSelected);
        dataManager.getShapeList().remove(currentSelected);
        setCurrentSelected(null);
        foolProof_Select_Remove();
        workspace.getRemove().setDisable(true);
        setPreIndex(-1);
        preColor = null;
        workspace.getGui().updateToolbarControls(false);
    }
    public void handleDrawShape(Pane rightPane,MouseEvent e){
      Workspace workspace = (Workspace) app.getWorkspaceComponent();
      DataManager dataManager = (DataManager) app.getDataComponent();
     
        if(rectangleMode == true){
           if(e.getEventType() == MouseEvent.MOUSE_EXITED){
               workspace.getWorkspace().setCursor(javafx.scene.Cursor.DEFAULT);
           }
           else if(e.getEventType() == MouseEvent.MOUSE_ENTERED){
               workspace.getWorkspace().setCursor(javafx.scene.Cursor.CROSSHAIR);
           }
          //get init x,y position when user pressed mouse
           else if(e.getEventType() == MouseEvent.MOUSE_PRESSED){
                //we should also get the rectangle apparence to current type
                setCurrentAppearance("rect");
                initX.set(e.getX());
                initY.set(e.getY());
           }
           //when use draw the mouse we change the length and width simutanously
           else if(e.getEventType() == MouseEvent.MOUSE_DRAGGED){
               finalX.set(e.getX());
               finalY.set(e.getY()); 
           }//should fullfill color and stroke, maybe evnet before this. 
           else if(e.getEventType() == MouseEvent.MOUSE_RELEASED){
               //clone this retangle
               Rectangle clone = new Rectangle();
               clone.setX(rect.getX());
               clone.setY(rect.getY());
               clone.setHeight(rect.getHeight());
               clone.setWidth(rect.getWidth());
               clone.setFill(dataManager.getCurrentFill());
               clone.setStroke(dataManager.getCurrentStrok());
               clone.setStrokeWidth(dataManager.getCurrentSliderValue());               
               
               rightPane.getChildren().add(clone);
               
               
               //make sure we can handle user shape selection
               clone.setOnMouseClicked(m -> handleShapeSelected(m,clone,rightPane));
               clone.setOnMouseDragged(m -> handleShapeSelected(m,clone,rightPane));
               
               //add clone to the shape list so we can store datas.
               dataManager.getShapeList().add(clone);
               //currentSelected = clone;
               foolProof_Select_Remove();
               
               //Hide the temp rectangle
               rect.setFill(null);
               rect.setStroke(null);
               rect.setStrokeWidth(0);
               initX.set(0);
               initY.set(0);
               finalX.set(0);
               finalY.set(0);
               
               workspace.getGui().updateToolbarControls(false);
           }
        }
        else if (ellipseMode == true){
          //get init x,y position when user pressed mouse
          if(e.getEventType() == MouseEvent.MOUSE_EXITED){
               workspace.getWorkspace().setCursor(javafx.scene.Cursor.DEFAULT);
           }
           else if(e.getEventType() == MouseEvent.MOUSE_ENTERED){
               workspace.getWorkspace().setCursor(javafx.scene.Cursor.CROSSHAIR);
           }
          
           else if(e.getEventType() == MouseEvent.MOUSE_PRESSED){
                setCurrentAppearance("ellip");
                initRadiusX.set(e.getX());
                initRadiusY.set(e.getY());
           }
           //when use draw the mouse we change the length and width simutanously
           else if(e.getEventType() == MouseEvent.MOUSE_DRAGGED){
               
               if((ellip.getCenterX() - (e.getX() - ellip.getCenterX())) > 0)
                    finalRadiusX.set(e.getX());
               if((ellip.getCenterY() - (e.getY() - ellip.getCenterY())) > 0)
                    finalRadiusY.set(e.getY()); 
           }//should fullfill color and stroke, maybe evnet before this. 
           else if(e.getEventType() == MouseEvent.MOUSE_RELEASED){
               //clone this ellipse
               Ellipse clone = new Ellipse();
               clone.setCenterX(ellip.getCenterX());
               clone.setCenterY(ellip.getCenterY());
               clone.setRadiusX(ellip.getRadiusX());
               clone.setRadiusY(ellip.getRadiusY());
               clone.setFill(dataManager.getCurrentFill());
               clone.setStroke(dataManager.getCurrentStrok());
               clone.setStrokeWidth(dataManager.getCurrentSliderValue());  
               rightPane.getChildren().add(clone);
               
               //make sure we can handle user shape selection
               clone.setOnMouseClicked(m -> handleShapeSelected(m,clone,rightPane));
               clone.setOnMouseDragged(m -> handleShapeSelected(m,clone,rightPane));
               //add clone to the shape list so we can store datas.
               dataManager.getShapeList().add(clone);               
            //   currentSelected = clone;
               
               foolProof_Select_Remove();
               //Hide the temp ellipse
               ellip.setFill(null);
               ellip.setStroke(null);
               ellip.setStrokeWidth(0);
               initRadiusX.set(0);
               initRadiusY.set(0);
               finalRadiusX.set(0);
               finalRadiusY.set(0);
               
               workspace.getGui().updateToolbarControls(false);
           }
        }
    }
    /*
    * handle dragging and selcting of already selected shape
    */
    public void handleShapeSelected(MouseEvent e,Shape clone,Pane rightPane){
        if(isSelectionMode() == true){
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            DataManager dataManager = (DataManager) app.getDataComponent();
            if(clone instanceof Rectangle){
                
                if(e.getEventType() == MouseEvent.MOUSE_DRAGGED){
                    if(e.getX() > 0 && e.getX() + ((Rectangle) clone).getWidth() < rightPane.getWidth())
                        ((Rectangle) clone).setX(e.getX());
                    if(e.getY() > 0 && e.getY() + ((Rectangle) clone).getHeight() < rightPane.getHeight())
                        ((Rectangle) clone).setY(e.getY());
                    workspace.getGui().updateToolbarControls(false);
                }
                else if(e.getEventType() == MouseEvent.MOUSE_CLICKED){
                    setCurrentSelected(clone);
                    foolProof_Select_Remove();
                    if(getPreIndex() >= 0){
                        handlePreSelect();
                    }
                    setPreIndex(dataManager.getShapeList().indexOf(currentSelected));
                    preColor = (Color)currentSelected.getStroke();
                    
               
                    workspace.getFillPicker().setValue((Color)(((Rectangle)currentSelected).getFill()));
                    dataManager.setCurrentFill((Color)(((Rectangle)currentSelected).getFill()));
                    workspace.getOutlinePicker().setValue((Color)(((Rectangle)currentSelected).getStroke()));
                    dataManager.setCurrentStrok((Color)(((Rectangle)currentSelected).getStroke()));
                    workspace.getStrokeThickness().setValue(((Rectangle)currentSelected).getStrokeWidth());
                    dataManager.setCurrentSliderValue(((Rectangle)currentSelected).getStrokeWidth());                    
                    
                    currentSelected.setStroke(Color.YELLOW);
                    workspace.getGui().updateToolbarControls(false);
                }
                
            }
            else if(clone instanceof Ellipse){
                
                if(e.getEventType() == MouseEvent.MOUSE_DRAGGED){
                    if(e.getX() - ((Ellipse)clone).getRadiusX() > 0 && e.getX() + ((Ellipse)clone).getRadiusX() < rightPane.getWidth())
                        ((Ellipse) clone).setCenterX(e.getX());
                    if(e.getY() - ((Ellipse)clone).getRadiusY() > 0 && e.getY() + ((Ellipse)clone).getRadiusY() < rightPane.getHeight())
                        ((Ellipse) clone).setCenterY(e.getY());
                    workspace.getGui().updateToolbarControls(false);
                }
                else if(e.getEventType() == MouseEvent.MOUSE_CLICKED){
                    setCurrentSelected(clone);
                    foolProof_Select_Remove();
                //    workspace.getRemove().setDisable(false);
                    if(getPreIndex() >= 0){
                        handlePreSelect();
                    }
                    setPreIndex(dataManager.getShapeList().indexOf(currentSelected));
                    preColor = (Color)clone.getStroke();
                    
                    workspace.getFillPicker().setValue((Color)(((Ellipse)currentSelected).getFill()));
                    dataManager.setCurrentFill((Color)(((Ellipse)currentSelected).getFill()));
                    workspace.getOutlinePicker().setValue((Color)(((Ellipse)currentSelected).getStroke()));
                    dataManager.setCurrentStrok((Color)(((Ellipse)currentSelected).getStroke()));
                    workspace.getStrokeThickness().setValue(((Ellipse)currentSelected).getStrokeWidth());
                    dataManager.setCurrentSliderValue(((Ellipse)currentSelected).getStrokeWidth());
                    
                    currentSelected.setStroke(Color.YELLOW);
                    workspace.getGui().updateToolbarControls(false);
                }
            }
        }
    }
    /*
    * This method move the shape to front
    */
    public void moveToFront(Pane rightPane){
        if(currentSelected != null){
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            DataManager dataManager = (DataManager) app.getDataComponent();
            rightPane.getChildren().remove(currentSelected);
            rightPane.getChildren().add(currentSelected);
            dataManager.getShapeList().remove(currentSelected);
            dataManager.getShapeList().add(currentSelected);
            foolProof_Select_Remove();
            setPreIndex(dataManager.getShapeList().size() - 1);
            workspace.getGui().updateToolbarControls(false);
        }
    }
    public void moveToBack(Pane rightPane){
        if(currentSelected != null){
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            DataManager dataManager = (DataManager) app.getDataComponent();
            rightPane.getChildren().remove(currentSelected);
            rightPane.getChildren().add(0,currentSelected);
            dataManager.getShapeList().remove(currentSelected);
            dataManager.getShapeList().add(0,currentSelected);
            foolProof_Select_Remove();
            setPreIndex(0);
            workspace.getGui().updateToolbarControls(false);
        }
    }
    
    
    
    /*
    * function to set the background color
    */
    public void handleBackgroundColor(ColorPicker cp,Pane rightPane){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        DataManager dataManager = (DataManager) app.getDataComponent();
        dataManager.setBackgroundC(cp.getValue());
        rightPane.setBackground(new Background(new BackgroundFill(dataManager.getBackgroundC(), CornerRadii.EMPTY, Insets.EMPTY)));
        workspace.getGui().updateToolbarControls(false);
    }
    /*
    * function to set the fill color
    */ 
    public void handleFillRect(ColorPicker cp){
        DataManager dataManager = (DataManager) app.getDataComponent();
        dataManager.setCurrentFill(cp.getValue());
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        if(currentSelected != null){
            if(currentSelected instanceof Rectangle)
                ((Rectangle)currentSelected).setFill(cp.getValue());
            else if(currentSelected instanceof Ellipse){
                ((Ellipse)currentSelected).setFill(cp.getValue());
            } 
        workspace.getGui().updateToolbarControls(false);
        }
    }
    /*
    * function to set the stroke color
    */
    public void handleOutlineColor(ColorPicker cp){
        DataManager dataManager = (DataManager) app.getDataComponent();
        dataManager.setCurrentStrok(cp.getValue());
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        //This action means shape is already selected
        if(currentSelected != null){
       /*     if(currentSelected instanceof Rectangle){
               // ((Rectangle)currentSelected).setStroke(cp.getValue());
              //  preColor = cp.getValue();
            }else if(currentSelected instanceof Ellipse){
         //       ((Ellipse)currentSelected).setStroke(cp.getValue());
            }  */
            preColor = (Color)cp.getValue();
            workspace.getGui().updateToolbarControls(false);
        }
     
    }
    /*
    *  use to set the stroke width of a shape
    */
    public void handleOutlineThickness(Slider s){
        DataManager dataManager = (DataManager) app.getDataComponent();
        dataManager.setCurrentSliderValue(s.getValue());
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        if(currentSelected != null){
            if(currentSelected instanceof Rectangle){
                if(((Rectangle)currentSelected).getX() - ((Rectangle)currentSelected).getStrokeWidth()> 0 
                        && ((Rectangle)currentSelected).getY() - ((Rectangle)currentSelected).getStrokeWidth()> 0)
                    ((Rectangle)currentSelected).setStrokeWidth(dataManager.getCurrentSliderValue());
                    
                }else if(currentSelected instanceof Ellipse)
                    if(((Ellipse)currentSelected).getCenterX()- ((Ellipse)currentSelected).getStrokeWidth()> 0 
                            && ((Ellipse)currentSelected).getCenterY()- ((Ellipse)currentSelected).getStrokeWidth()> 0)
                    ((Ellipse)currentSelected).setStrokeWidth(dataManager.getCurrentSliderValue());
             workspace.getGui().updateToolbarControls(false);
        }
    }
    /*
    * Set the rect/ellip to the appearance user wanted
    */
    public void setCurrentAppearance(String shape){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        DataManager dataManager = (DataManager) app.getDataComponent();
        if(shape.equals("rect")){
            workspace.getRightPane().getChildren().remove(rect);
            workspace.getRightPane().getChildren().add(rect);
            rect.setFill(dataManager.getCurrentFill());
        
            rect.setStroke(dataManager.getCurrentStrok());
            rect.setStrokeWidth(dataManager.getCurrentSliderValue());
        }
        else if(shape.equals("ellip")){
            workspace.getRightPane().getChildren().remove(ellip);
            workspace.getRightPane().getChildren().add(ellip);
            ellip.setFill(dataManager.getCurrentFill());
            ellip.setStroke(dataManager.getCurrentStrok());
            ellip.setStrokeWidth(dataManager.getCurrentSliderValue());
        }
    }
    public void handleSnapShot(Pane rightPane){
        FileManager fileManager = (FileManager)app.getFileComponent();
        WritableImage image = rightPane.snapshot(new SnapshotParameters(), null);
        try{
            fileManager.exportSnapShot(image);
        }catch(IOException ioe){
            
        }
    }
    public void handlePreSelect(){
        DataManager dataManager = (DataManager)app.getDataComponent();
         if( dataManager.getShapeList().get(getPreIndex()) instanceof Rectangle)
                ((Rectangle)dataManager.getShapeList().get(getPreIndex())).setStroke(preColor);
         else if( dataManager.getShapeList().get(getPreIndex()) instanceof Ellipse)
                ((Ellipse)dataManager.getShapeList().get(getPreIndex())).setStroke(preColor);
    }

    /**
     * @param currentSelected the currentSelected to set
     */
    public void setCurrentSelected(Shape currentSelected) {
        this.currentSelected = currentSelected;
    }

    /**
     * @return the preIndex
     */
    public int getPreIndex() {
        return preIndex;
    }

    /**
     * @param preIndex the preIndex to set
     */
    public void setPreIndex(int preIndex) {
        this.preIndex = preIndex;
    }

    /**
     * @return the selectionMode
     */
    public boolean isSelectionMode() {
        return selectionMode;
    }

    /**
     * @param selectionMode the selectionMode to set
     */
    public void setSelectionMode(boolean selectionMode) {
        this.selectionMode = selectionMode;
    }
}
