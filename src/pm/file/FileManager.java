package pm.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.TreeItem;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javax.imageio.ImageIO;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import pm.data.DataManager;
import pm.gui.Workspace;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class FileManager implements AppFileComponent {

    static final String SNAPSHOT = "./temp/images.png";
    static final String SHAPES = "shapes";
    static final String ELLIPSE = "ellipse";
    static final String RECTANGE = "rectange";
    static final String TYPE = "type";
    static final String XPOSITION = "xPosition";
    static final String YPOSITION = "yPosition";
    static final String WIDTH = "width";
    static final String HEIGHT = "height";
    static final String FILLCOLOR = "fillColor";
    static final String STROKECOLOR = "strokeColor";
    static final String STORKEWIDTH = "strokeWidth";
    
    static final String BACKGROUND = "background";
    static final String CURRENTFILL = "currentFill";
    static final String CURRENTSTROKE = "currentStroke";
    static final String CURRENTSTROKEWIDTH = "currentStrokeWidth";
    
    
    
    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        StringWriter sw = new StringWriter();
     
	// BUILD THE HTMLTags ARRAY
	DataManager dataManager = (DataManager)data;
        dataManager.changeLastColor();
    //    JsonArrayBuilder colorBuilder = Json.createArrayBuilder();
    //    buildColor(colorBuilder,dataManager.getBackgroundC());
    //    JsonArray colorArray = colorBuilder.build();
        
	JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        ArrayList<Shape> shape = dataManager.getShapeList();
        fillArrayWithShape(arrayBuilder,shape);
        JsonArray shapeArray = arrayBuilder.build();
	
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(BACKGROUND,dataManager.getBackgroundC().toString())
                .add(SHAPES,shapeArray)
		.build();

	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();

    }
    public void fillArrayWithShape(JsonArrayBuilder arrayBuilder, ArrayList<Shape> shape){
        for(int i = 0; i < shape.size(); i++){
            Shape temp = shape.get(i);
            JsonObject shapeObject = makeShapeJsonObject(temp);
            arrayBuilder.add(shapeObject);
        }
    }
    public JsonObject makeShapeJsonObject(Shape shape){
        if(shape instanceof Rectangle){
            Rectangle temp = (Rectangle)shape;
            JsonObject jso = Json.createObjectBuilder()
                    .add(TYPE,RECTANGE)
                    .add(XPOSITION,temp.getX())
                    .add(YPOSITION,temp.getY())
                    .add(WIDTH,temp.getWidth())
                    .add(HEIGHT,temp.getHeight())
                    .add(FILLCOLOR,temp.getFill().toString())
                    .add(STROKECOLOR,temp.getStroke().toString())
                    .add(STORKEWIDTH,temp.getStrokeWidth())
                    .build();
            return jso;
        }else{
            Ellipse temp = (Ellipse)shape;
            JsonObject jso = Json.createObjectBuilder()
                    .add(TYPE,ELLIPSE)
                    .add(XPOSITION,temp.getCenterX())
                    .add(YPOSITION,temp.getCenterY())
                    .add(WIDTH,temp.getRadiusX())
                    .add(HEIGHT,temp.getRadiusY())
                    .add(FILLCOLOR,temp.getFill().toString())
                    .add(STROKECOLOR,temp.getStroke().toString())
                    .add(STORKEWIDTH,temp.getStrokeWidth())
                    .build();
            return jso;
        }
    }
    public void buildColor(JsonArrayBuilder colorBuilder, Color c){
      //  JsonObject jso = Json.createObjectBuilder()
      //          .add(, BigDecimal.ONE)
      colorBuilder.add(c.getRed()).add(c.getGreen()).add(c.getBlue()).build();
    }
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error reading
     * in data from the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        DataManager dataManager = (DataManager)data;
    
        dataManager.getShapeList().clear();
        JsonObject jso = loadJSONFile(filePath);
    //    JsonObject backgroundC = jso.getJsonObject(BACKGROUND);
        String str = jso.getString(BACKGROUND);
        dataManager.setBackgroundC(Color.valueOf(str)); 
        //the array of all shapes
        JsonArray jsoA = jso.getJsonArray(SHAPES);
        for(int i = 0; i < jsoA.size(); i++){
            JsonObject shapeObject = jsoA.getJsonObject(i);
            String type = shapeObject.getString(TYPE);
            int x = shapeObject.getInt(XPOSITION);
            int y = shapeObject.getInt(YPOSITION);
            int xL = shapeObject.getInt(WIDTH);
            int yL = shapeObject.getInt(HEIGHT);
            Color fill = Color.valueOf(shapeObject.getString(FILLCOLOR));
            Color stroke = Color.valueOf(shapeObject.getString(STROKECOLOR));
            int strokeW = shapeObject.getInt(STORKEWIDTH);
            
            if(type.equals(RECTANGE)){
                Rectangle rect = new Rectangle(x,y,xL,yL);
                rect.setFill(fill);
                rect.setStroke(stroke);
                rect.setStrokeWidth(strokeW);
                dataManager.getShapeList().add(rect);
            }else{
                Ellipse ellip = new Ellipse(x, y, xL, yL);
                ellip.setFill(fill);
                ellip.setStroke(stroke);
                ellip.setStrokeWidth(strokeW);
                dataManager.getShapeList().add(ellip);
            }
        }
        //next time do reloadworkspace
        dataManager.reset();
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    /**
     * This method exports the contents of the data manager to a 
     * Web page including the html page, needed directories, and
     * the CSS file.
     * 
     * @param data The data management component.
     * 
     * @param filePath Path (including file name/extension) to where
     * to export the page to.
     * 
     * @throws IOException Thrown should there be an error writing
     * out data to the file.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {

    }
    
    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
	// NOTE THAT THE Web Page Maker APPLICATION MAKES
	// NO USE OF THIS METHOD SINCE IT NEVER IMPORTS
	// EXPORTED WEB PAGES
    }

    /*
    * This method export the snapshot
    */
    public void exportSnapShot(WritableImage image) throws IOException{
        File file = new File(SNAPSHOT);
        ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
    }
}
